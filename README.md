Projekt - Model informačního systému
---------
Body: 21/25

Varianta 28. Letiště
---------
Navrhněte informační systém pro letiště, který bude schopen evidovat lety. Každému cestujícímu je na základě letenky vydána palubní vstupenka na určitý let a místo v letadle. Různá letadla (i stejného typu) mají různý počet a rozmístění míst. Místo může být v letadlo u okýnka, u uličky, či uprostřed a může být v různé třídě (turistická, business, první, ...). U každého letadla je evidován výrobce, typ, datum výroby, datum poslední revize, počet členů posádky. U letu je evidován čas odletu, očekávaná doba letu, terminál a číslo gatu. Každý gate má přiřazen typy letadel, které z něj mohou odlétat. Předpokládejte, že se jedná o větší letiště s více terminály, na každém terminálu je více než jeden gate.